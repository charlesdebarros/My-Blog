### My Personal Website

After a long wait I finally decided to put together a website. The project includes a blog, a space to publish my projects, and also a space to have my CV displayed.

I intend to have the wire-framing, prototyping, photoshop files and plain HTML/CSS files uploaded to this repo. I need to compress them first as the files are quire large. They will eventually be available.

As I have trained on Ruby and Ruby on Rails, I have chosen them as my options to build this project.

My website can is hosted on [Heroku](https://www.heroku.com/) and can be seen [here](https://charlesdebarros.herokuapp.com/).

***

![image](https://raw.githubusercontent.com/charlesdebarros/My-Blog/master/app/assets/images/screenshot.jpg)

***

### Technologies used

* Ruby 2.5.X
* Rails 5.0.X
* PostgreSQL 9.6.X
* Sass-Rails 5.0.X
* Ruby Gems:
  - Redcarpet 3.3
  - Pygments 0.6.3
  - Frindly ID 5.1
  - Will Paginate 3.0
  - Mail Form 1.5
  - Devise 3.5
  - Rails_12factor 0.0.3
